pulse-notification
==================

Mail to push notification gateway for ADT Pulse notifications.

I'll write a blogpost on this at some point, but for now...

## Getting started

I have a .forward file calling this script, specifying my local copy of the yaml file as the first (and only) argument to the script.

Get a [Pushover](https://pushover.net) account, install the app, and create an application. Make a copy of sample.yaml and fill it out. `notify_keys` is a list of user keys to send the notifications to, `app_key` is your application's key from Pushover. `user_names` is a mapping of the panel codes to human names so you can have something meaningful in your notification. (The website lets you map codes to names, I don't know why it doesn't include them in the notifications).

I'm thinking of offering this as a hosted service for those who don't run their own mailservers, find me if you're interested.

