# Pulse email to pushover notification tool
#
# Copyright (c) 2012 Mark Walling, mark@markwalling.org
#

require "mail"
require "tempfile"
require "net/https"
require "yaml"

mail_body = STDIN.read

temp_file = Tempfile.new('adtmail')
temp_file.open
temp_file.write(mail_body)
temp_file.close

mail = Mail.read(temp_file.path)

mail_body = mail.body.decoded

temp_file.delete

config = YAML::load_file(ARGV[0])

priority = 0

if mail_body.include? "Security System Armed Stay"
    message = "Armed Stay"
elsif mail_body.include? "Security System Disarmed"
    message = "Disarmed"
elsif mail_body.include? "Security System Armed Away"
    message = "Armed Away"
elsif mail_body.include? "Attempt to set Security Panel Arm Mode"
    message = "Pulse failed to change state"
elsif mail_body.include? "Security Panel AC power restored"
    message = "AC Power Restored"
elsif mail_body.include? "Security Panel AC power loss"
    message = "AC Power Fail"
elsif mail_body.include? "BURGLARY ALARM"
    message = "Alarm!"
    priority = 1
    if mail_body =~ /Sensor: (.*) \(/
        message += " "
        message += $1
    end
elsif mail_body.include? "Alarm cleared"
    message= "Alarm cleared"
    priority = 1
else
    message = "Donno..."
end

if mail_body =~ /access code (\d+)/
    message += " by "
    message += config['user_names'].fetch($1.to_i) { |uid| "unknown user #{uid}" }
end

config['notify_keys'].each do |user_key|
    url = URI.parse("https://api.pushover.net/1/messages")
    req = Net::HTTP::Post.new(url.path)
    req.set_form_data({
        :token => config['app_key'],
        :user => user_key,
        :message => message,
        :priority => priority,
    })
    res = Net::HTTP.new(url.host, url.port)
    res.use_ssl = true
    res.verify_mode = OpenSSL::SSL::VERIFY_PEER
    res.start {|http| http.request(req) }
end

